pragma solidity >=0.4.21 <0.7.0;// version que usaremos para compilar el proyecto

contract Election { //iniciando el contracto
    string public candidate;

    //Modelo para los candidatos
    struct Candidate {
        uint id;
        string name;
        string description;
        string position;
        uint voteCount;
    }

    //Donde vamos a guardar todos esos candidatos
    mapping (uint=>Candidate) public candidates;
    //Donde guardaremos a los votantes
    mapping (address=>bool) public voters;
    //Esta variable es para saber cuantos candidatos hay
    uint public candidatesCount;

    constructor() public{
        addCandidate('Candidato 1','lkasjlkjalksdjaslkdjkjalskd','president');
        addCandidate('Candidato 2','lkasjlkjalksdjaslkdjkjalskd','president');
        addCandidate('Candidato 3','lkasjlkjalksdjaslkdjkjalskd','president');
    }

    //Funciones de la logica
    function addCandidate(string memory _name, string memory _description,string memory _position) private{
        candidatesCount++;
        candidates[candidatesCount] = Candidate(candidatesCount,_name,_description,_position,0);
    }

    function vote(uint _candidate) public {
        //VALIDA QUE NO HAYA VOTADO ANTES, ES DECIR SOLO 1 VOTO
        // require(!voters[msg.sender]);
        //que sea valido
        require(_candidate>0);
        //se guarda el voto
        voters[msg.sender] = true;
        //se actualiza la cuenta de los votos
        candidates[_candidate].voteCount++;
    }
}