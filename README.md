# Descipcion
este proyecto sera una implementacion de una pequeña Dapp para llevar el control
de votaciones entre candidatos.

para una documentacion mas profunda acerca del proyecto, seguir los pasos e instalacion
de requerimientos, por favor seguir este tutorial(https://www.dappuniversity.com/articles/the-ultimate-ethereum-dapp-tutorial).

## Notas personales.
las pruebas son necesarias, ya que cada transaccion realizada en la red representa
un valor, valor que sera **descontado directamente** de la cuenta o wallet que se tenga
de **Ethereum**, pero como sera trabajado en un entorno controlado (local) y el
tutorial funciona 100%, en caso de que alguna prueba de error, pueden obviarla.